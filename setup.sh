#!/bin/bash
echo "# MongoDB setup from www.genecrucean.com and https://bitbucket.org/crewshin/mongodb" >> ~/.bash_profile
echo "export PATH=/Applications/MongoDB/bin:\$PATH" >> ~/.bash_profile
mkdir -p /Applications/MongoDB/db
mkdir -p /Applications/MongoDB/logs